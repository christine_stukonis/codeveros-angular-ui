// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const path = require('path');

try {
  const puppeteer = require('puppeteer');
  process.env.CHROME_BIN = puppeteer.executablePath();
  console.log('Using Puppeteer provided Headless Chrome');
} catch (e) {
  console.log('Puppeteer not installed, trying to use separately installed Chrome');
}

module.exports = function (config) {
  config.set({

    // OWASP Proxy
    // proxies: {
    //     "/*": {
    //       // "target": "http://jenkins-demo-zap:8080",
    //       "target": "http://52.203.157.75:30363/",
    //       "changeOrigin": true
    //     }
    // },

    browserDisconnectTimeout: 20000,
    browserNoActivityTimeout: 60000,
    browserDisconnectTolerance: 5,
    retryLimit: 5,
    basePath: 'src',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-junit-reporter',
      'karma-coverage-istanbul-reporter',
      '@angular-devkit/build-angular/plugins/karma'
    ],
    coverageIstanbulReporter: {
      dir: path.join(__dirname, './reports/coverage'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true,
      'report-config': {
        html: {
          subdir: 'lcov-report'
        }
      }
    },
    junitReporter: {
      outputFile: path.resolve(__dirname, './reports/junit.xml')
    },
    reporters: ['progress', 'junit'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['CustomChrome'],
    customLaunchers: {
      CustomChrome: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--disable-gpu',
          '--disable-web-security',
          '--disable-setuid-sandbox',
          '--remote-debugging-port=9222',
          //TODO: figure out zap proxy config
          // '--headless',
          // '--proxy-server=10.152.183.162:8080',
          //'--proxy-server=jenkins-demo-zap:8080',
          // Use proxy for localhost URLs
          //'--proxy-bypass-list=<-loopback>',
        ]
      }
    },
    singleRun: true
  });
};
